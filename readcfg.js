const fs = require('fs');
const args = process.argv;

/*thought process:
 * load default cfg -> keyvalue pair
 *		key = command, value = value
 *
 * next make new blank keyvalue pair, only this is a double keyvalue
 * key1:(key2:value)
 * where key1 = command
 * key2 = command value
 * value = count
 *
 * go though each config individually:
 *		does it differ to default (or isn't in default)?
 *		if so then:
 *		if no key exists then add appropirate key:(key:1)
 *		if it does exist then increase key:key by 1
 *
 */
var player;
var def = {};
var vals = {};
var amnt = {};

reader('_default', true);
var plist = playerlist();
plist.forEach(function(p) {
	//console.log(p);
	reader(p,false);
});

finalconfig();
//console.log(amnt);
/*


var topnum;
var topval;
console.log("//from 40 pro configs (and the default config), the following config is generated from");
console.log("//no comment => this is a default config value, which majority of pros do not change");
for (var key in amnt){
	if(amnt[key]>19 || !(key in def)){
		topnum = 0;
		for (var kz in vals[key]){
			if(vals[key][kz]>topnum){
				topnum = vals[key][kz];
				topval = kz;
			}
		}
		console.log(""+key+"\""+topval+"\" //pro useage: "+(100*topnum/40)+"%");
	}else{
		console.log(""+key+"\""+def[key]+"\"");
	}
}
*/
//write the config

//find top 10
/*

var topvals = [];
var topiden = [];
for(let i=0;i<10;i++){
	topvals.push(0);
	topiden.push(0);
}
var count;
*/
/*for (var key in amnt){
	//console.log(vals[key]);
	count = 0;
	while(amnt[key]<=topvals[count] && count <10){
		count++;
	}
	if(count>9) continue;
	topvals[count]=amnt[key];
	topiden[count]=key;
}*/
/*
for (var key in amnt){
	//console.log(vals[key]);
	count = 0;
	while((amnt[key]-Object.keys(vals[key]).length)<=topvals[count] && count <10){
		count++;
	}
	if(count>9) continue;
	topvals[count]=(amnt[key]-Object.keys(vals[key]).length);
	topiden[count]=key;
}


//print messages
console.log("total players cfgs was: "+plist.length);
console.log();
for(let i=0;i<10;i++){
	console.log(topiden[i]+": "+topvals[i])
	console.log(vals[topiden[i]]);
	console.log();
}
*/


function reader(player, dflag){
	var data = fs.readFileSync('cfgs/'+player,'utf8');
	var user
	try {
	  user = JSON.parse(data)
	} catch(err) {
	  console.error(err)
	}
	if (dflag){
		def = user;
	}else{
		for(var key in user){
			if(key == 'controls') continue;
			if(def[key]!=user[key]){
				if(key in vals){
					amnt[key]++;
					if(user[key] in vals[key]){
						vals[key][user[key]]++;
					}else{
						vals[key][user[key]]=1;
					}
				}else{
					vals[key] = {};
					amnt[key] =1;
					vals[key][user[key]]=1;
				}
			}
		}
	}
	return;
}

function finalconfig(){
	var data = fs.readFileSync('cfgs/'+'_default','utf8');
	var maxnum;
	var maxname;
	var user
	try {
		user = JSON.parse(data);
	} catch(err) {
		console.error(err)
	}
	for(var key in user){
		if(key == 'controls') continue;
		if(amnt[key]>17){
			maxnum = 0;
			for(v in vals[key]){
				if(vals[key][v]>maxnum){
					maxnum = vals[key][v];
					maxname = v;
				}
			}
			process.stdout.write('"'+key+'":'+maxname+",");
		}else{
			process.stdout.write('"'+key+'":'+user[key]+',');
		}
	}
	console.log();
}

function playerlist(){
	return [
		'blankfrosty',
		'blncr',
		'bozott',
		'brandor2',
		'color',
		'Cuffuffles',
		'equasus',
		'flued',
		'hoppcx',
		'itsgrizz',
		'itsjuitziii',
		'jamemesg',
		'jcstani',
		'johnnyder',
		'kari',
		'kloutbayashi',
		'meetya',
		'mosswi',
		'nxgd',
		'pmv',
		'riderrr',
		'roadman',
		'rumi',
		'shirraishiez',
		'sillyjap',
		'sirbaka',
		'snuffalo',
		'stazza',
		'sumika',
		'themagictool',
		'trdingo',
		'verru',
		'void',
		'vokus',
		'xxnonutterxx'
	];
}

